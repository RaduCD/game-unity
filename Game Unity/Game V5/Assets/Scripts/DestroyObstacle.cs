﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObstacle : MonoBehaviour
{
    public GameObject player;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }
    void Update()
    {
        if (transform.position.z < player.transform.position.z - 4f)
            Destroy(gameObject);
    }
}
