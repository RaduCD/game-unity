﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Transform player;
    public Text scoreText;
    static public string staticScoreText;

    void Update()
    {
        scoreText.text = player.position.z.ToString("0");
       
    }

    private void OnDisable()
    {
        staticScoreText = scoreText.text;
    }

    public string GetScoreText()
    {
        return staticScoreText;
    }
}
