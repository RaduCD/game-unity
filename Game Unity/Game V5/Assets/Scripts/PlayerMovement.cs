﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rigidbody;
    public float fowardForce = 2000f;
    public float sideForce = 500f;
    public float maxSpeed = 200f;

    private bool moveLeft = false, moveRight = false;

    void Update()
    {
        if (Input.GetKey("d") || Input.GetKey(KeyCode.RightArrow))
        {
            moveRight = true;
        }
        else
            moveRight = false;

        if (Input.GetKey("a") || Input.GetKey(KeyCode.LeftArrow))
        {
            moveLeft = true;
        }
        else
            moveLeft = false;

    }

    void FixedUpdate()
    {
        rigidbody.AddForce(0, 0, fowardForce * Time.deltaTime);

        if (rigidbody.velocity.magnitude > maxSpeed)
        {
            rigidbody.velocity = rigidbody.velocity.normalized * maxSpeed;
        }

        if (moveRight) 
        {
            rigidbody.AddForce(sideForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if (moveLeft) 
        {
            rigidbody.AddForce(-sideForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if(rigidbody.position.y < -1f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }

    }
}
